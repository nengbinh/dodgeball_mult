const express = require('express');
const app = express(); 
const server = app.listen(1337);
app.use(express.static(__dirname));
const io = require('socket.io')(server); 
console.log('listen on port 1337')

// Server information
let serverStatus = 'waiting' // |start
let serverPlayer = {} //save all player information
let serverHostID = ''     // determind whos the host
let serverHostName = ''

//
io.on('connection', function(socket) {
    // when player leave
    socket.on('disconnect', function(){
        for(let key in serverPlayer){
            if(serverPlayer[key]['socket'] == socket.id){
                delete(serverPlayer[key]) // delete from playerlist
                socket.leave('NextGame') // delete from group
                // check if is host
                if(key == serverHostName){
                    // get a random host
                    for(let key1 in serverPlayer){
                        serverHostName = key1
                        break
                    }
                }
                //if in waiting list
                if(serverStatus == 'waiting'){ 
                    socket.broadcast.to('NextGame').emit("addToGroupResult",{stat: true, data:serverPlayer, host:serverHostName})
                }
            }
        }
        // check if empty
        let count = 0
        for(let key in serverPlayer){
            count ++
        }
        if(!count) clear()
    })
    // leave room
    socket.on('leave', function(){
        socket.leave('NextGame')
    })
    // when player connected
    socket.on('addToGroup', function(data) {
        // check if name exist
        if(serverPlayer.hasOwnProperty(data)){
            socket.emit('addToGroupResult', {stat:false, msg:`Name exist please provide another name`})
            return 
        }
        // check if the list was empty
        if( Object.keys(serverPlayer).length == 0 ){
            // then he will become the host
            serverHostID = socket.id
            serverHostName = data
        }
        // check Server if on waiting
        if(serverStatus != 'waiting'){
            socket.emit('addToGroupResult', {stat:false, msg:`Sorry, Can't join, there's a game Going on`})
        }else{
            // check if the host

            // can join
            serverPlayer[data] = {socket: socket.id}
            socket.join('NextGame')
            io.to('NextGame').emit("addToGroupResult",{stat: true, data:serverPlayer, host:serverHostName}) // send to all player
        }
    })

    // when received a start request
    socket.on('startTheGame', function(data){
        if(serverStatus == 'waiting' && data.name == serverHostName){
            serverStatus = 'start'
            startCountDown(3, data)
        }
    })
    // startCountDown
    function startCountDown(count, data) {
        if(count >= 0){
            let temp = `Game starting in ${count} sec`
            io.to('NextGame').emit("startTheGameResult",{stat:false, msg:temp})
            setTimeout(() => { startCountDown(count - 1, data) }, 1000);
        }else{
            io.to('NextGame').emit("startTheGameResult",{stat:true, player: serverPlayer, data:data})
        }
    }

    ////////////    from host

    socket.on('CreateBall', function(data){
        socket.broadcast.to('NextGame').emit("CreateBall_Back",data)
    })

    socket.on('ballChange', function(data){
        socket.broadcast.to('NextGame').emit("ballChange_Back",data)
    })

    socket.on('playerMove', function(data){
        socket.broadcast.to('NextGame').emit("playerMove_Back",data)
    })

    socket.on('playerHit', function(data){
        io.to('NextGame').emit("playerHit_Back",data)
    })

    socket.on('GameOver', function(data){
        clear()
    })

    function clear() {
        serverStatus = 'waiting'
        serverHostID = ''
        serverHostName = ''
        serverPlayer = {}
    }
    
})
