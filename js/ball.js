class Ball{
    constructor(width, height) {
        // first create ball number
        this.data = []
        this.width = width // canvas rect
        this.height = height
    }
    // create ball
    create(num) {
        for(let i=0; i<num; i++) {
            this.data.push({
                x: this.random_position(),
                y: this.random_position(),
                x_move: this.random_speed(),
                y_move: this.random_speed(),
                radius: this.random_radius(),
                color: this.random_color()
            })
        }
        return this.data
    }
    // set ball data
    setData(data) { this.data = data }

    // check if touch
    check_touch(index) {
        let change = false
        let radius = this.data[index].radius
        if( (this.data[index].x - radius) <= 0) {
            change = 'x-left'
        }
        if( (this.data[index].x + radius) >= this.width){
            change = 'x-right'
        }
        if( (this.data[index].y - radius) <= 0) {
            change = 'y-top'
        }
        if( (this.data[index].y + radius) >= this.height){
            change = 'y-bottom'
        }
        
        return change
    }

    // change the speed
    change_speed(chan, index) {
        let speed = this.random_speed() 
        if(chan == 'x-left'){
            this.data[index].x_move = speed
        }else if(chan == 'x-right'){
            speed = -speed
            this.data[index].x_move = speed
        }else if(chan == 'y-top'){
            this.data[index].y_move = speed
        }else if(chan == 'y-bottom'){
            speed = -speed
            this.data[index].y_move = speed
        }
    }
    
    // change the radius
    change_radius(index){
        this.data[index].radius = this.random_radius()
    }
    // change the color
    change_color(index) {
        this.data[index].color = this.random_color()
    }
    // random position
    random_position() {
        return  Math.random() * this.width
    }
    // random speed
    random_speed() {
        let rand = Math.ceil(Math.random() * 20)
        if(rand == 1){
            return Math.ceil(Math.random() * 10) + 10
        }else if(rand == 2){
            return Math.ceil(Math.random() * 6) + 5
        }else{
            return Math.ceil(Math.random() * 4) + 2
        }
    }
    // random radius
    random_radius() {
        let rand = Math.ceil(Math.random() * 30)
        if(rand == 1){
            return Math.ceil(Math.random() * 20) + 10
        }else if(rand == 2){
            return Math.ceil(Math.random() * 20) + 5
        }else{
            return Math.ceil(Math.random() * 10) + 3
        }
        
    }
    // random color
    random_color() {
        var col = [0, 1, 2];
        col[0] = Math.random() * 100 + 155;
        col[0] = col[0].toFixed();
        col[1] = Math.random() * 100 + 155;
        col[1] = col[1].toFixed();
        col[2] = Math.random() * 100 + 155;
        col[2] = col[2].toFixed();
        var num = Math.floor(Math.random() * 3);
        col[num] = 0;
        return col.join(',');
    }
}