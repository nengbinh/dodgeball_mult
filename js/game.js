window.onload = function(){   
    // local setting
    let canvas
    $('#wating_button').on('click',function(){
        start()
    })

    // init socket io
    const socket = io(); 
    
    // ask for name
    let playerName
    
    // asking box
    init(socket)

    if(playerName == null){
        init(socket)
    }


    // host info
    let host 

    // sends name to server
    socket.on('connect', ()=>{
        socket.emit('addToGroup', playerName)
    })

    // received response from addToGroup result
    socket.on('addToGroupResult', data=>{
        // check if join
        if(data['stat']){
            // sucess
            document.getElementById('Cover').style.display='block'
            document.getElementById('wating').style.display='block'
            //
            host = data['host']
            // check if is host
            if(playerName == data['host']){
                document.getElementById('set_box').style.display='block'
            }else{
                document.getElementById('set_box').style.display='none'
            }
            // player list
            let temp = ''
            for(let key in data['data']){
                // check if host
                if(key == data['host']){
                    temp += `<li><font color='#0f0'>${key}</font> (HOST)</li>`
                }else{
                    temp += `<li>${key}</li>`
                }
            }
            document.getElementById('wating_list').innerHTML = temp
        }else{
            // fail
            alert(data['msg'])
            window.location.href = '/'
        }
    })

    // startGameResult
    socket.on('startTheGameResult', function(data){
        if(data['stat']){
            document.getElementById('wating_text').innerHTML = 'start....'
            game(data['player'], data['data'])
        }else{
            document.getElementById('wating_text').innerHTML = data['msg']
        }
    })
    
    // start button
    function start() {
        let obj = {}
        obj.name = playerName
        obj.sec = parseInt(document.getElementById('set_sec').value)
        obj.add = parseInt(document.getElementById('sec_add').value)
        obj.bounds = document.getElementById('set_bounds').checked
        socket.emit('startTheGame', obj)
    }

    // enter Game
    function game(player, data) {
        // unset the cover
        document.getElementById('Cover').style.display = 'none'
        document.getElementById('wating').style.display = 'none'
        // let the game start
        canvas = new Canvas('mycanvas','mydiv',socket, data)
        console.log(playerName,host,player)
        canvas.gameStart(playerName, host, player)
    }
    
    // name
    function init(socket) {
        let name = prompt('Provide a name (6 Characters Max)','')
        if(name){
            if(name.length > 6) name = name.substring(0, 6)
            playerName = name
        }else{
            init(socket)
        }
    }


    ///////// onload end
}

