class Canvas{
    //
    constructor(dom, div, socket, initsetting) {
        // basic setting
        this.socket = socket
        this.deltatime = 0
        this.lasttime = Date.now()
        this.ball_time = 0  // timer for ball to be added
        this.ball_addtime = initsetting.sec * 1000
        this.ball_add = initsetting.add
        this.ball_bounds = initsetting.bounds
        console.log(this.ball_addtime, this.ball_add)
        console.log(this.ball_bounds)
        this.ball_num_init = 5
        // time
        this.sec = 0
        this.secR = 0
        //ball and player object
        this.ball
        this.player
        // get the style
        let temp = getComputedStyle(document.getElementById(div))
        this.width = temp.width.substr(0, temp.width.length-2)
        this.height = temp.height.substr(0, temp.height.length-2)
        let left = temp.left
        // mycanvas set
        this.dom = document.getElementById(dom)
        this.ctx = this.dom.getContext('2d')
        this.dom.width = this.width
        this.dom.height = this.height
        this.dom.style.left = left
        // add my player into array
        this.myplayer
        this.gameOver = false
        //
    }

    // set name
    setName(name){
        // save name
        this.myplayer = name
    }

    // init press key
    initPressKey() {
        this.Key = {
            UP: false,
            DOWN: false,
            LEFT: false,
            RIGHT: false
        };
          
        this.DIRECTION = {
            87: "UP",
            68: "RIGHT",
            83: "DOWN",
            65: "LEFT"
        };
    }

    // game start
    gameStart(name, host, playerlist) {
        // set name
        this.setName(name)
        //
        this.ball = new Ball(this.width, this.height)
        // check if host
        if(name == host){
            this.isHost = true
            // create some balls first
            let balldata = this.ball.create(this.ball_num_init)
            this.socket.emit('CreateBall', balldata) // send to server
        }else{
            this.isHost = false
            this.socketNotHost_event()
        }
        this.socketALL_event()
        //
        this.player = new Player(this.width, this.height)
        // create my character
        this.player.add_player(playerlist)
        
        // bind key
        this.keyBind()
        //
        this.initPressKey()

        // start the timer for draw all
        this.draw_all()
    }

    // socket listen
    socketNotHost_event() {

        // when data from host

        this.socket.on('CreateBall_Back', (data)=>{
            this.ball.setData(data)
        })

        this.socket.on('ballChange_Back', (res)=>{
            this.ball.data[res.key] = res.data
        })

    }

    // socket listen
    socketALL_event() {

        this.socket.on('playerMove_Back', (data)=>{
            this.player.move(data.name, data.dir)
        })

        this.socket.on('playerHit_Back', (res)=>{
            console.log('here')
            this.player.data[res.name].alive = false
            this.player.data[res.name]['dieTime'] = res.time
        })
        

    }

    // full draw
    draw_all() {
        // check if gameover 
        if(this.gameOver){
            this.gameover_it()
        }
        // time set
        let now = Date.now()
        this.deltatime = now - this.lasttime
        this.lasttime = now
        if(this.deltatime>50) this.deltatime=50
        //clear
        this.ctx.clearRect(0, 0, this.width, this.height)
        // draw
        this.add_ball()
        this.draw_ball()
        //
        this.draw_time()
        //
        this.draw_player()
        this.playerHit()
        this.move();
        // set and check all player
        if(this.player.check_status()){
            // send to server
            this.socket.emit('GameOver')
            // leave room
            this.socket.emit('leave')
            // show result
            setTimeout(() => {
                // all die
                this.result(this.player.data)
            }, 150);
        }else{
            // keep going
            window.requestAnimationFrame(()=>{
                this.draw_all()
            })
        }
    }

    // show result out
    result(data) {
        // sort the key
        let key = Object.keys(data).sort(function(a, b) {
            return data[b].dieTime - data[a].dieTime;
        });
        //
        let count = 0
        let temp = ''
        for(let val of key){
            // winner
            if(count == 0){
                document.getElementById('result_winner').innerHTML = `The winner is [${val}]`
            }
            // list
            count ++
            temp += `<li>${count} - ${val} ( alive time ${data[val].dieTime} sec )</li>`
        }
        document.getElementById('result_list').innerHTML = temp
        document.getElementById('result').style.display = 'block'
        document.getElementById('Cover').style.display = 'block'
    }

    // check if ball touch eachother
    ball_detected(key) {
        //
        let ball = this.ball.data[key]
        let change = false
        // check with each one
        for(let [key1,val1] of this.ball.data.entries()){
            if (ball !== val1) { // cant compare with its self
                // check touch
                if( this.ball_distance(ball, val1, key) ){
                    this.ball.data[key1].x_move *= -1
                    this.ball.data[key1].y_move *= -1
                    this.ball.data[key].x_move *= -1
                    this.ball.data[key].y_move *= -1
                    if(this.isHost){
                        change = true
                    }
                }
            }
        }
        // change
        if(change){
            this.socket.emit('CreateBall', this.ball.data)
        }
    }

    // check ball distance
    ball_distance(now, target, key) {
        if(now == undefined) return false
        return Math.sqrt(Math.pow(now.x - target.x, 2) + Math.pow(now.y - target.y, 2)) <= now.radius + target.radius + 5
    }

    // draw ball out
    draw_ball() {
        this.ctx.save()
        this.ctx.lineWidth = 3 
        this.ctx.shadowBlur = 10
        this.ctx.shadowColor = 'white'
        //
        for(let [key,val] of this.ball.data.entries()){
            // check touch
            if(this.ball_bounds) this.ball_detected(key)
            // move
            this.ball.data[key].x += val.x_move * (this.deltatime/20)
            this.ball.data[key].y += val.y_move * (this.deltatime/20)
            // check only if host
            if(this.isHost){
                // check if touch edge
                let change = this.ball.check_touch(key)
                if(change){
                    // change speed
                    this.ball.change_speed(change, key)
                    // change color
                    this.ball.change_color(key)
                    // change radius
                    this.ball.change_radius(key)
                    this.socket.emit('ballChange',{data:this.ball.data[key], key:key}) // send to server
                }
            }
            // draw
            this.ctx.beginPath()
            this.ctx.arc(val.x, val.y, val.radius, 0, Math.PI * 2)
            this.ctx.closePath()
            this.ctx.fillStyle = `rgba(${val.color}, 1)`
            this.ctx.fill()
        }
        //
        this.ctx.restore()
    }

    // draw player
    draw_player() {
        this.ctx.save()
        this.ctx.lineWidth=5;
        this.ctx.strokeStyle='black';
        // for name
        this.ctx.font = '10px Verdana' 
        this.ctx.textAlign = 'center'
        //
        for(let key in this.player.data){
            let val = this.player.data[key]
            this.ctx.save()
            //
            this.ctx.beginPath();
            this.ctx.translate(val.x, val.y)
            if(key == this.myplayer){ // check if is my character
                this.ctx.fillStyle='orange';
            }else{
                this.ctx.fillStyle='#f00';
            }
            // check if alive
            if(!val.alive) {
                this.ctx.fillStyle = 'gray'
            }
            // text
            this.ctx.save()
            if(!val.alive) {
                this.ctx.fillStyle = 'gray'
            }else{
                this.ctx.fillStyle = 'yellow'
            }
            this.ctx.fillText(key, -10, 10)
            this.ctx.restore()
            //
            this.ctx.rect(-20, -20, 20, 20);
            //
            this.ctx.stroke();
            this.ctx.fill();
            // draw eyes
            this.ctx.beginPath();
            this.ctx.fillStyle = 'white'
            this.ctx.arc(-14, -13, 3, 0, Math.PI * 2)
            this.ctx.arc(-6, -13, 3, 0, Math.PI * 2)
            this.ctx.fill();
            //
            this.ctx.restore()
        }
        //
        this.ctx.restore()
    }

    // key bind
    keyBind() {
        
        document.onkeydown = e =>{
            if(this.DIRECTION[e.keyCode]) {
                this.Key[this.DIRECTION[e.keyCode]] = true; 
            }
        }

        document.onkeyup = e => {
            if(this.DIRECTION[e.keyCode]){
                this.Key[this.DIRECTION[e.keyCode]] = false;
            }
        }
    }

    // player move
    move(){
        // setting
        let name = this.myplayer
        let dir 
        // base on key to determind which way
        if(this.Key['UP'] && this.Key['RIGHT']) {
            dir = 'UP_RIGHT'

        }else if(this.Key['UP'] && this.Key['LEFT']) {
            dir = 'UP_LEFT'

        }else if(this.Key['DOWN'] && this.Key['LEFT']) {
            dir = 'DOWN_LEFT'

        }else if(this.Key['DOWN'] && this.Key['RIGHT']) {
            dir = 'DOWN_RIGHT'

        }else if(this.Key['UP']) {
            dir = 'UP'

        }else if(this.Key['DOWN']) {
            dir = 'DOWN'

        }else if(this.Key['LEFT']) {
            dir = 'LEFT'

        }else if(this.Key['RIGHT']) {
            dir = 'RIGHT'
        }
        // move it
        if(dir){
            this.player.move(name, dir)
            this.socket.emit('playerMove',{dir:dir,name:name})
        }
    }

    // draw the time out
    draw_time(){
        this.secR += this.deltatime
        if(this.secR >= 1000){
            this.secR -= 1000
            this.sec += 1
        }
        let w = this.width
        let h = this.height
        this.ctx.save()
        this.ctx.font = '30px Verdana' 
        this.ctx.textAlign = 'center'
        this.ctx.shadowBlur = 10  
        this.ctx.shadowColor = 'red' 
        this.ctx.fillStyle = 'white'
        // 绘制出文字
        this.ctx.fillText('TIME: '+this.sec, w * 0.5, h - 20)
        this.ctx.restore()
    }
    
    // check player if hit balls
    playerHit() {
        // check if player die
        let p = this.player.data[this.myplayer]
        if(p['alive']){
            //
            for(let val of this.ball.data){
                // check
                let cal = this.RectCircleColliding(
                    {x:val.x,y:val.y,r:val.radius}, 
                    {x:p.x-10,y:p.y-10,w:20,h:20})
                // if it collding
                if (cal) {
                    this.player.data[this.myplayer].alive = false
                    this.gameOver = true
                    this.gameover_it()
                    // send to server
                    this.socket.emit('playerHit', {name:this.myplayer,time:this.sec})
                }
            }
        }
    }

    // rect and circle colliding
    RectCircleColliding(circle,rect){
        var distX = Math.abs(circle.x - rect.x-rect.w/2);
        var distY = Math.abs(circle.y - rect.y-rect.h/2);
    
        if (distX > (rect.w/2 + circle.r)) { return false; }
        if (distY > (rect.h/2 + circle.r)) { return false; }
    
        if (distX <= (rect.w/2)) { return true; } 
        if (distY <= (rect.h/2)) { return true; }
    
        var dx=distX-rect.w/2;
        var dy=distY-rect.h/2;
        return (dx*dx+dy*dy<=(circle.r*circle.r));
    }
    
    gameover_it() {
        document.onkeydown = null
        document.onkeyup = null
        this.player.die(this.myplayer)
        this.initPressKey()
    }

    //add ball into game
    add_ball() {
        this.ball_time += this.deltatime
        if(this.ball_time >= this.ball_addtime) {
            this.ball_time -= this.ball_addtime
            this.ball.create(this.ball_add)
            this.socket.emit('CreateBall', this.ball.data) // send to server
        }
    }

    

    
}